try:
    A: int = int(input("Dame un número entero no negativo: "))
    B: int = int(input("Dame otro: "))
except:
    print("Los números deben ser enteros.")

if A > B:
    A, B = B, A

if (A < 0) or (B < 0):
    print("Alguno o ambos son negativos.")

suma=0
if (A > 0) and (B > 0):
    for numero in range(A, B+1):
        if (numero % 2) == 1:
            suma = suma + numero
    print(suma)
